# TDD in Python

## How it works

1. Install [pipenv](https://pipenv.readthedocs.io/en/latest/install/#installing-pipenv) into your computer
2. Install dependencies using `pipenv install`
3. Execute tests using `pipenv run pytest`

##  Documentation

### Pytest

Is the tool we will use for testing.

- **Main documentation:** https://docs.pytest.org/en/latest/contents.html
- **Introduction:** https://stackabuse.com/test-driven-development-with-pytest/
- **Assertions:** https://docs.pytest.org/en/latest/assert.html
- **Fixtures:** https://docs.pytest.org/en/latest/fixture.html
- **Fixture example:** https://pythontesting.net/framework/pytest/pytest-fixtures-easy-example/  
